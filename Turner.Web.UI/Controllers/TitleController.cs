﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Turner.BusinessLogic;
using Turner.Model;

namespace Turner.Web.UI.Controllers
{
    public class TitleController : Controller
    {
 
        /// <summary>
        /// Search Title by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public ActionResult Index(string name)
        {
            TitleBC titleBC = new TitleBC();
            List<Title> model = titleBC.GetByName(name);
            return View(model);
        }

        /// <summary>
        /// Get title details by id
        /// </summary>
        /// <param name="titleId"></param>
        /// <returns></returns>
        public ViewResult Detail(int titleId)
        {
            TitleBC titleBC = new TitleBC();
            Title model = titleBC.GetById(titleId);
            return View(model);
        }

    }
}
