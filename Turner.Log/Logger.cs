﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Turner.Log
{
/// <summary>
/// Log the details 
/// </summary>
 
    public class Logger
    {
        #region constant
        private const string LOG_FOLDER_PATH ="c:\\log\\";
        #endregion

        #region public method
        /// <summary>
        /// Handles error by accepting the error message
        /// </summary>
        /// <param name="error"></param>
        public static void LogException(Exception error)
        {
                string filepath = LOG_FOLDER_PATH + DateTime.Today.ToString("dd-MM-yy") + ".txt";
                if (!File.Exists(filepath))
                {
                    File.Create(filepath).Close();
                }
                using (StreamWriter w = File.AppendText(filepath))
                {
                    w.WriteLine("\r\nLog Entry : ");
                    w.WriteLine("{0}", DateTime.Now.ToString());
                    w.WriteLine("Error message: " + error.Message);
                    w.WriteLine(error.StackTrace);
                    w.WriteLine("__________________________");
                    w.Flush();
                    w.Close();
                }
        }
        #endregion
    }

}
